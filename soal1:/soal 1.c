#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

pthread_t tid1, tid2, tid3, tid4;
pid_t child;
int statusz;

void unzip(char *newFile) {
	int status;
	pid = fork ();
	
	char destfile[100];
	char origin[100] = "/home/azka/modul3/";
	
	strcat(origin, newFile);
    strcpy(destFile, origin);
    strcat(origin, ".zip");
    
    if(child == 0) {
		char *argv[] = {"unzip", "-o", origin, "-d", destFile, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
        }
	}
	
	void* do_unzip() {
		pthread_t id = pthread_self();
		
		if(pthread_equal(id, tid[0])) {
			unzip("quote");
			}
    		else if(pthread_equal(id, tid[1])) {  
        	unzip("music");
			}
			
	return NULL;
}

void newLine(char* dir) {
    int status;
    pid_t child_id = fork();

    if(child_id < 0) {
        exit(EXIT_FAILURE);
        }
    	else if(child_id == 0) {
        char text[100] = "echo "" >> ";
        strcat(text, dir);
        execl("/bin/sh", "sh", "-c", text, (char *)0);
        } 
    	else {
        while((wait(&status)) > 0);
        }
	}

	void base64(char* text) {
		int status;
    	pid_t child_id = fork();

    	if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0) {
        execl("/bin/sh", "sh", "-c", text, (char *)0);
    }
    else {
        while((wait(&status)) > 0);
    }
}

void* decode() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[2])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/azka/modul3/quote/q%d.txt >> /home/azka/modul3/quote.txt", i);
            base64(cmd);
            newLine("/home/azka/modul3/quote.txt");
        }
    }
    
    else if(pthread_equal(id, tid[3])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/azka/modul3/music/m%d.txt >> /home/azka/modul3/music.txt", i);
            base64(cmd);
            newLine("/home/azka/modul3/music.txt");
        }
    }
}


